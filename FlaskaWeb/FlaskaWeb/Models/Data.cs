namespace FlaskaWeb.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public class Data
    {
        public int Id { get; set; }

        public double Volume { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime DateTime { get; set; }
    }
}
