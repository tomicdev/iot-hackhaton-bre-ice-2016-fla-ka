﻿using FlaskaWeb.Models;
using System;
using System.Text;
using System.Web.Http;
using Newtonsoft.Json;
using Microsoft.Azure.Devices.Client;
using System.Threading.Tasks;
using System.Linq;

namespace FlaskaWeb.Controllers
{

    [RoutePrefix("Api/Data")]
    public class DataController : ApiController
    {

        static DeviceClient deviceClient;
        string iotHubUri = "FlaskaHub.azure-devices.net";
        string deviceKey = "lcjgwQPeQkbxt5ummHaND9bLOugoQ90TurSBEbSkFng=";

        [HttpGet]
        public IHttpActionResult Get() {
            using (var context = new DataContext())
            {
                if(context.Datas.Count() == 0)
                {
                    return StatusCode(System.Net.HttpStatusCode.NoContent);
                }
                Data d = context.Datas.OrderByDescending(e => e.DateTime).Take(1).ToArray()[0];
                return Ok(d);
            }
        }

        [Route("All")]
        [HttpGet]
        public IHttpActionResult GetAll()
        {
            using (var context = new DataContext())
            {
                var d = context.Datas.ToList();
                return Ok(d);
            }
        }

        [HttpPost]
        public async Task<IHttpActionResult> Post(Data data)
        {
            System.Diagnostics.Debug.WriteLine(data.Volume);
            data.DateTime = DateTime.UtcNow;

            if (data == null)
                return StatusCode(System.Net.HttpStatusCode.BadRequest);

            using (var context = new DataContext())
            {
                context.Datas.Add(data);
                context.SaveChanges();
            }
           
              Console.WriteLine("Simulated device\n");
              deviceClient = DeviceClient.Create(iotHubUri, new DeviceAuthenticationWithRegistrySymmetricKey("myFirstDevice", deviceKey));
           
              await SendDeviceToCloudMessagesAsync(data);
        
              return Ok();
         }

        private async Task<object>  SendDeviceToCloudMessagesAsync(Data data)
        {

            var messageString = JsonConvert.SerializeObject(data);
            var message = new Message(Encoding.ASCII.GetBytes(messageString));

            await deviceClient.SendEventAsync(message);
            return null;
        }

        [HttpDelete]
        public IHttpActionResult Delete()
        {
            using (var context = new DataContext())
            {
                context.Database.ExecuteSqlCommand("TRUNCATE TABLE [Data]");
                return Ok();
            }
        }

    }
}
