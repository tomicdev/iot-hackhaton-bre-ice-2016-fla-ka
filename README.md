# README #

## Smart bottle ##

We decided to use ultrasonar technology to determine the level of the water in the container.
Data is sent to a raspberry pi and from it, to a SQL database hosted on Azure.
The data is then used to inform how much water we have left in our container.

With this information we can easily know if we should drink more water, leading to increased health.

### Usage ###

Visit [Smart Bottle](http://flaskaweb.azurewebsites.net/Bottle).

### Team ###

Our team consists of Peter Colarič, Matjaž Mav, Marko Murgelj and Aleksander Tomič.