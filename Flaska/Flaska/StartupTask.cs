﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Windows.ApplicationModel.Background;
using Windows.Devices.Gpio;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Threading;
using System.Net.Http;

// 169.254.145.159

// The Background Application template is documented at http://go.microsoft.com/fwlink/?LinkID=533884&clcid=0x409

namespace Flaska
{
    public sealed class StartupTask : IBackgroundTask
    {
        private const int SAMPLE_SIZE = 25;
        private const int SAMPLING_RATE = 40;

        private const double MAX_DISTANCE = 8.3;
        private const double MIN_DISTANCE = 2.2;

        private SerialTemperature serialTemp { get; set; }
        private double currentTemp { get; set; }
        private double currentHum { get; set; }

        private GpioPin pin17;
        private Timer timer;
        private Stopwatch stopwatch;
        private bool trigger;
        private List<double> data;
        private List<double> sample;

        public void Run(IBackgroundTaskInstance taskInstance)
        {
            //FeedService.SendToAzure();
            //TemperatureInit();
            Init();

            while (true) ;
        }

        private void Init()
        {
            var gpio = GpioController.GetDefault();
            stopwatch = new Stopwatch();
            trigger = false;
            data = new List<double>();
            sample = new List<double>();

            if (gpio == null)
            {
                return;
            }

            pin17 = gpio.OpenPin(17);
            pin17.ValueChanged += SIG_ValueChanged;

            timer = new Timer(SendTrigger, null, 0, SAMPLING_RATE);
        }

        private void SendTrigger(object sender)
        {
            pin17.SetDriveMode(GpioPinDriveMode.Output);
            pin17.Write(GpioPinValue.High);
            trigger = true;
            Task.Delay(new TimeSpan(200)).Wait();
            pin17.Write(GpioPinValue.Low);
            pin17.SetDriveMode(GpioPinDriveMode.Input);
        }

        private void SIG_ValueChanged(GpioPin sender, GpioPinValueChangedEventArgs args)
        {
            if (!trigger)
                return;

            if (sender.Read() == GpioPinValue.High)
            {
                stopwatch.Start();
            }
            else if (stopwatch.IsRunning)
            {
                stopwatch.Stop();
                if (sample.Count < SAMPLE_SIZE)
                {
                    sample.Add(stopwatch.ElapsedTicks / (double)850);
                }
                if (sample.Count == SAMPLE_SIZE)
                {
                    double distance_cm = Math.Round(sample.Sum() / SAMPLE_SIZE, 1);
                    double tmp = distance_cm - MIN_DISTANCE;
                    tmp = (tmp < 0) ? 0 : tmp;
                    tmp = (tmp > MAX_DISTANCE - MIN_DISTANCE) ? MAX_DISTANCE - MIN_DISTANCE : tmp;
                    tmp = tmp / (MAX_DISTANCE - MIN_DISTANCE);
                    tmp = 1 - tmp;

                    // dodajanje
                    var client = new HttpClient();
                    // Dictionary<string, string> values = new Dictionary<string, string>();
                    //FormUrlEncodedContent content = new FormUrlEncodedContent(values);
                    HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, new Uri("http://flaskaweb.azurewebsites.net/Api/Data"));
                    request.Content = new StringContent("{\"Volume\": " + tmp + " }",
                                                        Encoding.UTF8,
                                                        "application/json");

                    var result =  client.SendAsync(request);
                    result.Wait();
                    
                    sample.Clear();
                }
                stopwatch.Reset();
                trigger = false;
            }
        }

        //private void TemperatureInit()
        //{
        //    serialTemp = new SerialTemperature();
        //    System.ComponentModel.BackgroundWorker bgw = new System.ComponentModel.BackgroundWorker();
        //    bgw.DoWork += Bgw_DoWork;
        //    bgw.RunWorkerAsync();
        //}

        //private void Bgw_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        //{
        //    while(true) {
        //        var tmp = serialTemp.GetData();
        //        if(tmp != null)
        //        {
        //            currentTemp = tmp[0];
        //            currentHum = tmp[1];
        //        }
        //        Task.Delay(5000);
        //    }
            
        //}
    }
}