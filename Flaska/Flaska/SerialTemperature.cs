﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Devices.Gpio;
using Windows.Devices.Enumeration;
using Windows.Devices.SerialCommunication;
using Windows.Storage.Streams;
using System.Threading;
using System.Diagnostics;

namespace Flaska
{
    public sealed class SerialTemperature
    {

        private SerialDevice SerialPort { get; set; }
        const uint maxReadLength = 1024;

        public SerialTemperature()
        {
            string aqs = SerialDevice.GetDeviceSelector("UART0");                                       /* Find the selector string for the serial device   */
            var dis = DeviceInformation.FindAllAsync(aqs).GetAwaiter().GetResult();                     /* Find the serial device with our selector string  */
            SerialPort = SerialDevice.FromIdAsync(dis[0].Id).GetAwaiter().GetResult();                  /* Create an serial device with our selected device */

            /* Configure serial settings */
            SerialPort.WriteTimeout = TimeSpan.FromMilliseconds(1000);
            SerialPort.ReadTimeout = TimeSpan.FromMilliseconds(1000);
            SerialPort.BaudRate = 9600;
            SerialPort.Parity = SerialParity.None;
            SerialPort.StopBits = SerialStopBitCount.One;
            SerialPort.DataBits = 8;
            

        }

        
        public IList<double> GetData()
        {
            /* Read data in from the serial port */
            try
            {
                DataReader dataReader = new DataReader(SerialPort.InputStream);
                uint bytesToRead = dataReader.LoadAsync(maxReadLength).GetAwaiter().GetResult();
                string rxBuffer = dataReader.ReadString(bytesToRead);
                rxBuffer = rxBuffer.Split(';').First();
                var tempRx = (double)Convert.ToDouble(rxBuffer.Split(',')[0]);
                var distRx = (double)Convert.ToDouble(rxBuffer.Split(',')[1]);

                return new List<double>() { tempRx, distRx };
                
            } catch
            {
                return null;
            }

           
        }
    }   
       
}
