﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace FlaskaWeb.Models
{
    public class DataContext :DbContext
    {
        public DataContext() : base("BazaConnectionString")
        {
        }

        public virtual DbSet<Data> Datas { get; set; }
    }
}